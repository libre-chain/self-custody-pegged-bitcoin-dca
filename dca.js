const { Api, JsonRpc } = require('eosjs');
const fetch = require('node-fetch');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig');
const { TextDecoder, TextEncoder } = require('util');

const dotEnv = require("dotenv");
dotEnv.config();
dotEnv.config({ path: ".env" });

const apiserver = process.env.APISERVER;
const from = process.env.ACCOUNT;
const to = process.env.TO;
const quantity = process.env.QUANTITY;
const memo = 'exchange: BTCUSD, 0.000000001 PBTC, DCA';

const rpc = new JsonRpc(apiserver, { fetch });
const privateKeys = [process.env.PRIVATEKEY];

const signatureProvider = new JsSignatureProvider(privateKeys);
const eos = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });


async function transfer () {
    const result = await eos.transact({
        actions: [{
            account: 'usdt.ptokens',
            name: 'transfer',
            authorization: [{
              actor: from,
              permission: 'active',
            }],
            data: {
                from: from,
                to: to,
                quantity: quantity,
                memo: memo
            }
          }]
        }, {
          blocksBehind: 3,
          expireSeconds: 30,
        })
    console.log(result)
}

transfer()
